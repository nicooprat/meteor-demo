Todos = new Mongo.Collection('todos');

Todos.schema = new SimpleSchema({
  text: {
    type: String,
  },
  checked: {
    type: Boolean,
    defaultValue: false
  }
});

Todos.attachSchema(Todos.schema);
Todos.attachBehaviour('sortable');
Todos.attachBehaviour('softRemovable');