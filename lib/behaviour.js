CollectionBehaviours.define('sortable', function(opts = {}) {

  const collection = this.collection;

  // Options

  const options = _.defaults(opts, this.options, {
    field: 'position'
  });

  // Schema

  const definition = {};

  definition[options.field] = {
    type: Number,
    optional: true,
    defaultValue: 0
  };

  collection.attachSchema(new SimpleSchema(definition));

  // Hooks

  collection.before.insert((userId, doc) => {
    var count = collection.find().count();
    doc[options.field] = count;
  });

  collection.helpers({
    previous() {
      var current = this[options.field];
      return collection.findOne({
        [options.field]: current - 1
      });
    },

    next() {
      var current = this[options.field];
      return collection.findOne({
        [options.field]: current + 1
      });
    },

    up(direct = true) {
      if( this.previous() ) {
        if( direct ) {
          this.previous().down(false);
        }

        collection.update(this._id, {
          $set: {
            [options.field]: this[options.field] - 1
          }
        });
      }
    },

    down(direct = true) {
      if( this.next() ) {
        if( direct ) {
          this.next().up(false);
        }

        collection.update(this._id, {
          $set: {
            [options.field]: this[options.field] + 1
          }
        });
      }
    }
  })
});