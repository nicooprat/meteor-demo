Template.todos.helpers({
  todos: function() {
    return Todos.find({}, {
      sort: {
        position: 1
      }
    });
  }
});

Template.todos.events({
  'submit form': function (e) {
    e.preventDefault();

    var $form = $(e.target),
        text = $form.find('[name=text]').val();

    Todos.insert({
      text
    });

    $form[0].reset();
  },

  'change :checkbox': function (e) {
    var todoId = e.target.name,
        checked = e.target.checked;

    Todos.update(todoId, {
      $set: {
        checked: checked
      }
    });
  },

  'click .up': function(e) {
    var todoId = e.target.dataset.id

    Todos.findOne(todoId).up();
  },

  'click .down': function(e) {
    var todoId = e.target.dataset.id

    Todos.findOne(todoId).down();
  },

  'click .delete': function(e) {
    var todoId = e.target.dataset.id;

    Todos.softRemove(todoId);
  }
});